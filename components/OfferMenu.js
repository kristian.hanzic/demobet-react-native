import React from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import OfferMenuButton from './OfferMenuButton';
import {
  faAlignLeft,
  faBaseball,
  faBasketball,
  faCalendar,
  faHomeAlt,
  faSearch,
  faSoccerBall,
  faVolleyball,
} from '@fortawesome/free-solid-svg-icons';

const OfferMenu = () => {
  return (
    <View style={styles.offer_menu_scroll_wrapper}>
      <ScrollView
        style={styles.offer_menu_scroll}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        <View style={styles.offer_menu}>
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faSearch}
            text="PRETRAŽI"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faCalendar}
            text="KALENDAR"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faAlignLeft}
            text="LIGE"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faHomeAlt}
            text="SPORTOVI"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faSoccerBall}
            text="NOGOMET"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faBasketball}
            text="KOŠARKA"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faBaseball}
            text="TENIS"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faVolleyball}
            text="ODBOJKA"
          />
          <OfferMenuButton
            style={styles.offer_menu_button}
            icon={faVolleyball}
            text="STOLNI TENIS"
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  offer_menu: {
    flexDirection: 'row',
  },
  offer_menu_scroll: {},
  offer_menu_button: {},
});

export default OfferMenu;
