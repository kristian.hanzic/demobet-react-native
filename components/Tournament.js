import React, {useState} from 'react';
import {StyleSheet, View, Text, TouchableHighlight} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChartColumn} from '@fortawesome/free-solid-svg-icons';

const Tournament = () => {
  const [tournament, setTournament] = useState({
    title: 'FC Noah - FC Pyunik',
    information: '3216     10.03.2022. 12:00     +6',
    odds1: 1.15,
    odds2: 2.54,
  });

  return (
    <View style={styles.tournament}>
      <View style={styles.tournament_header}>
        <View style={styles.tournament_header_title_content}>
          <Text style={styles.tournament_header_title_text}>
            {tournament.title}
          </Text>
          <Text style={styles.tournament_header_title_information}>
            {tournament.information}
          </Text>
        </View>
        <View style={styles.tournament_header_analytics}>
          <FontAwesomeIcon
            style={styles.tournament_header_analytics_icon}
            icon={faChartColumn}
          />
        </View>
      </View>
      <View style={styles.tournament_odds}>
        <TouchableHighlight
          style={styles.tournament_odds_1_touchable}
          underlayColor="black">
          <View style={styles.tournament_odds_1}>
            <View style={styles.tournament_odds_1_title}>
              <Text style={styles.tournament_odds_1_title_text}>1</Text>
            </View>
            <View style={styles.tournament_odds_1_coefficient}>
              <Text style={styles.tournament_odds_1_coefficient_text}>
                {tournament.odds1}
              </Text>
            </View>
          </View>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.tournament_odds_1_touchable}
          underlayColor="black">
          <View style={styles.tournament_odds_1}>
            <View style={styles.tournament_odds_2_title}>
              <Text style={styles.tournament_odds_1_title_text}>1</Text>
            </View>
            <View style={styles.tournament_odds_2_coefficient}>
              <Text style={styles.tournament_odds_1_coefficient_text}>
                {tournament.odds2}
              </Text>
            </View>
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  tournament_header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#222222',
  },
  tournament_header_title_content: {
    padding: 10,
    flex: 1,
  },
  tournament_header_title_text: {
    fontSize: 14,
    color: 'white',
  },
  tournament_header_title_information: {
    fontSize: 12,
    color: '#999798',
  },
  tournament_header_analytics_icon: {
    marginRight: 20,
    color: '#999798',
  },
  tournament_odds: {
    flexDirection: 'row',
  },
  tournament_odds_1_touchable: {
    flex: 1,
    flexDirection: 'column',
  },
  tournament_odds_1: {},
  tournament_odds_1_title: {
    padding: 3,
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#404040',
    borderRightWidth: 1,
    borderRightColor: '#404040',
    backgroundColor: '#171717',
  },
  tournament_odds_1_title_text: {
    fontSize: 12,
    color: '#999798',
  },
  tournament_odds_1_coefficient: {
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: '#404040',
    borderBottomWidth: 1,
    borderBottomColor: '#404040',
    backgroundColor: '#222222',
  },
  tournament_odds_2_title: {
    padding: 3,
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#404040',
    backgroundColor: '#171717',
  },
  tournament_odds_2_coefficient: {
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#404040',
    backgroundColor: '#222222',
  },
  tournament_odds_1_coefficient_text: {
    padding: 8,
    fontSize: 16,
    color: 'white',
  },
});

export default Tournament;
