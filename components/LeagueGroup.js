import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faSoccerBall} from '@fortawesome/free-solid-svg-icons';
import Tournament from './Tournament';

const LeagueGroup = ({title, time}) => {
  return (
    <View style={styles.offer_container}>
      <View style={styles.offer_group}>
        <View style={styles.offer_group_title}>
          <FontAwesomeIcon
            style={styles.offer_group_title_icon}
            icon={faSoccerBall}
          />
          <Text style={styles.offer_group_title_text}>{title}</Text>
        </View>
        <View style={styles.offer_group_separator} />
        <View style={styles.offer_group_time}>
          <Text style={styles.offer_group_time_text}>{time}</Text>
        </View>

        <Tournament />
        <Tournament />
      </View>
    </View>
  );
};

LeagueGroup.defaultProps = {
  title: 'Nepoznata liga',
  time: 'Nepoznato vrijeme',
};

const styles = StyleSheet.create({
  offer_group_title: {
    flexDirection: 'row',
    backgroundColor: '#000000',
  },
  offer_group_title_icon: {
    margin: 7,
    color: 'white',
  },
  offer_group_title_text: {
    padding: 5,
    color: 'white',
  },
  offer_group_separator: {
    height: 3,
    backgroundColor: '#008148',
  },
  offer_group_time: {
    backgroundColor: '#363636',
  },
  offer_group_time_text: {
    padding: 5,
    fontSize: 12,
    color: 'white',
  },
});

export default LeagueGroup;
