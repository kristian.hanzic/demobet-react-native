import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableHighlight} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

const OfferMenuButton = props => {
  const [styles, setStyles] = useState([
    StyleSheet.create({
      offer_menu_button: {
        flexDirection: 'row',
        width: 90,
        height: 55,
      },
      offer_button_content: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#1F1F1F',
      },
      offer_separator: {
        width: 1,
        height: 55,
        backgroundColor: '#363636',
      },
      font_awesome_icon: {
        marginTop: 10,
        color: '#9D9D9C',
      },
      text: {
        color: '#9D9D9C',
      },
    }),
    StyleSheet.create({
      offer_menu_button: {
        flexDirection: 'row',
        width: 90,
        height: 55,
      },
      offer_button_content: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'red',
      },
      offer_separator: {
        width: 1,
        height: 55,
        backgroundColor: '#363636',
      },
      font_awesome_icon: {
        marginTop: 10,
        color: '#9D9D9C',
      },
      text: {
        color: '#9D9D9C',
      },
    }),
  ]);

  const [style, setStyle] = useState(0);

  return (
    <TouchableHighlight onPress={() => alert(props.text)} underlayColor="black">
      <View style={styles[style].offer_menu_button}>
        <View style={styles[style].offer_button_content}>
          <FontAwesomeIcon
            style={styles[style].font_awesome_icon}
            icon={props.icon}
          />
          <Text style={styles[style].text}>{props.text}</Text>
        </View>
        <View style={styles[style].offer_separator} />
      </View>
    </TouchableHighlight>
  );
};

export default OfferMenuButton;
