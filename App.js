import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import {SvgCssUri} from 'react-native-svg';
import OfferMenu from './components/OfferMenu';
import LeagueGroup from './components/LeagueGroup';

const YourApp = () => {
  return (
    <View style={styles.container}>
      <View style={styles.main_navigation}>
        <View style={styles.navigation_logo_wrapper}>
          <SvgCssUri
            style={styles.logo}
            width="150"
            uri="https://www.arenacasino.hr/media/2021-05/arenabet-logo.svg"
          />
        </View>

        <View style={styles.navigation_buttons_wrapper}>
          <SvgCssUri
            style={styles.message_icon}
            width="30"
            height="30"
            uri="https://www.arenacasino.hr/static/arenacasino2020/images/chat-colored-silver.svg"
          />
          <SvgCssUri
            style={styles.hamburger_icon}
            width="30"
            height="30"
            uri="https://www.svgrepo.com/show/391275/menu-hamburger.svg"
          />
        </View>
      </View>
      <OfferMenu />

      <ScrollView>
        <LeagueGroup title="ENGLESKA - 1.LIGA" time="Regularno vrijeme" />
        <LeagueGroup
          title="MEĐUNARODNE - SP KVALIFIKACIJE"
          time="Regularno vrijeme"
        />
        <LeagueGroup title="NJEMAČKA 1.LIGA" time="Regularno vrijeme" />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  main_navigation: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 12,
    paddingBottom: 12,
    backgroundColor: '#050505',
  },
  navigation_logo_wrapper: {
    paddingLeft: 20,
  },
  navigation_buttons_wrapper: {
    paddingRight: 20,
    flexDirection: 'row',
  },
  logo: {},
  message_icon: {
    marginRight: 20,
    color: 'white',
  },
  hamburger_icon: {
    color: 'white',
  },
});

export default YourApp;
